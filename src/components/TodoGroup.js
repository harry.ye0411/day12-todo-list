// TodoGroup.js

import React from 'react';
import { useSelector } from 'react-redux';
import TodoItem from './TodoItem';
import '../css/TodoItem.css';



const TodoGroup = () => {
    const todos = useSelector((state) => state.todos);

    return (
        <div className="TodoGroup">
            {todos.map((todo) => <TodoItem key={todo.id} todo={todo} />)}
        </div>
    );
};

export default TodoGroup;