// TodoItem.js

import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleTodo, deleteTodo } from '../app/todosSlice';

const TodoItem = ({ todo }) => {
    const dispatch = useDispatch();

    return (
        <div
            className={`TodoItem ${todo.finished ? 'finished' : ''}`}
            onClick={() => dispatch(toggleTodo(todo.id))}
        >
            {todo.text}
            <button onClick={(e) => { e.stopPropagation(); dispatch(deleteTodo(todo.id)); }}>
                X
            </button>
        </div>
    );
};

export default TodoItem;