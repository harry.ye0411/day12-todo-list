// TodoGenerator.js

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../app/todosSlice';
import '../css/TodoGenerator.css'

const TodoGenerator = () => {
    const [input, setInput] = useState('');
    const dispatch = useDispatch();

    const handleAdd = () => {
        if (!input) return;
        dispatch(addTodo(input));
        setInput('');
    };

    const handleKeyDown = (e) => {
        if (e.keyCode === 13) {
            handleAdd();
        }
    };

    return (
        <div className="TodoGenerator">
            <input
                value={input}
                onChange={(e) => setInput(e.target.value)}
                onKeyDown={handleKeyDown}
            />
            <button onClick={handleAdd} disabled={!input}>Add</button>
        </div>
    );
};

export default TodoGenerator;