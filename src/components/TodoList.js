// TodoList.js

import React from 'react';
import TodoGroup from './TodoGroup';
import TodoGenerator from './TodoGenerator';
import '../css/TodoList.css';

const TodoList = () => {
    return (
        <div className="centered">
            <div className="todo-box">
                <h1>Todo List</h1>
                <TodoGroup />
                <TodoGenerator />
            </div>
        </div>
    );
};

export default TodoList;