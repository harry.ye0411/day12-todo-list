import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

export const todosSlice = createSlice({
    name: 'todos',
    initialState: [],
    reducers: {
        addTodo: (state, action) => {
            state.push({ id: uuidv4(), text: action.payload, finished: false });
        },
        toggleTodo: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload);
            state[index].finished = !state[index].finished;
        },
        deleteTodo: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload);
            state.splice(index, 1);
        },
    },
});

export const { addTodo, toggleTodo, deleteTodo } = todosSlice.actions;

export default todosSlice.reducer;


